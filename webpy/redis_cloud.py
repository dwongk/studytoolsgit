from redis import Redis
from os import getenv

redis = Redis(
    host=getenv('REDIS_HOST', 'redis'),
    port=getenv('REDIS_PORT', 6379),
    password=getenv('REDIS_PW', None))
