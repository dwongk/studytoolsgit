from web.template import CompiledTemplate, ForLoop, TemplateResult


# coding: utf-8
def index (divisions, books, categories, title):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(title, True))
    extend_([u'<h1>', escape_(title, True), u'</h1>\n'])
    __lineoffset__ -= 3
    def generate_table(books, categories):
        self = TemplateResult(); extend_ = self.extend
        extend_([u'<table cellpadding="4" width="100%" style="border: 1px solid #000000; border-collapse: collapse;" border="1">\n'])
        count = len(books)
        width = 100 / count
        extend_([u'<colgroup span="', escape_(count, True), u'" width="', escape_((width), True), u'%">\n'])
        extend_([u'<thead>\n'])
        extend_([u'<tr>'])
        for entry in loop.setup(books):
            extend_([u'<th><a href="', escape_((entry), True), u'/', escape_((entry), True), u'">', escape_(entry, True), u'</a></th>'])
        extend_([u'</tr>\n'])
        extend_([u'</thead>\n'])
        extend_([u'<tbody>\n'])
        for row in loop.setup(categories):
            extend_([u'<tr>'])
            for entry in loop.setup(books):
                extend_([u'<td><a class="', escape_(row, True), u'" href="', escape_((entry), True), u'/', escape_((row), True), u'">', escape_(row, True), u'</a></td>'])
            extend_([u'</tr>\n'])
        extend_([u'</tbody>\n'])
        extend_([u'</table>\n'])
        return self
    extend_([u'<h2>Individual Books</h2>\n'])
    extend_([escape_(generate_table(books, categories), False)])
    extend_([u'<h2>Combined Books by Divisions</h2>\n'])
    extend_([escape_(generate_table(divisions, categories), False)])

    return self

index = CompiledTemplate(index, 'templates\\index.html')
join_ = index._join; escape_ = index._escape

# coding: utf-8
def layout (content):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'<!DOCTYPE html>\n'])
    extend_([u'<html lang="en">\n'])
    extend_([u'<head>\n'])
    extend_([u'<meta charset="UTF-8">\n'])
    extend_([u'<title>', escape_(content.title, True), u'</title>\n'])
    extend_([u'<link rel="stylesheet" href="https://storage.googleapis.com/bb2017-152608.appspot.com/static/category.css" type="text/css" charset="utf-8"/>\n'])
    extend_([u'</head>\n'])
    extend_([u'<body>\n'])
    extend_([escape_(content, False), u'\n'])
    extend_([u'</body>\n'])
    extend_([u'</html>\n'])

    return self

layout = CompiledTemplate(layout, 'templates\\layout.html')
join_ = layout._join; escape_ = layout._escape

# coding: utf-8
def send_file (content, title):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(title, True))
    extend_([escape_(content, False), u'\n'])

    return self

send_file = CompiledTemplate(send_file, 'templates\\send_file.html')
join_ = send_file._join; escape_ = send_file._escape

# coding: utf-8
def send_table (words, verses, title):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(title, True))
    extend_([u'<table cellpadding="4" width="100%" style="border: 1px solid #000000; border-collapse: collapse;" border="1">\n'])
    extend_([u'<col width=""><col width="73%">\n'])
    for w, v in loop.setup(zip(words, verses)):
        extend_([u'<tr><td>', escape_(w, False), u'</td><td>', escape_(v, False), u'</td></tr>\n'])
    extend_([u'</table> '])

    return self

send_table = CompiledTemplate(send_table, 'templates\\send_table.html')
join_ = send_table._join; escape_ = send_table._escape

