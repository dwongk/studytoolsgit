from __future__ import print_function
# import sys; sys.path.append('lib')
from gevent import monkey; monkey.patch_all()
from gevent.pywsgi import WSGIServer
import web  # pylint:disable=import-error
import os
from redis_cloud import redis
import json

urls = ("/", "index",
        '/static/(.*)', 'Static',
        '/form', 'Form',
        '/(.*)', 'send_table')

render = web.template.render('templates', base='layout', globals={'zip': zip})


class index(object):
    def GET(self):
        pipe = redis.pipeline()
        pipe.get('divisions')
        pipe.get('books')
        pipe.smembers('categories')
        pipe.get('title')
        divisions, books, categories, title = pipe.execute()
        divisions = json.loads(divisions)
        books = json.loads(books)
        # categories -= books
        return render.index(divisions, books, sorted(categories), title)


class Form(object):
    def GET(self):
        return render.hello_form()

    def POST(self):
        form = web.input(name='Nobody', greet='Hello')
        greeting = "%s, %s" % (form.greet, form.name)
        return render.hello_index(greeting=greeting)


class Static:
    def GET(self, file):
        try:
            with open('static/' + file, 'rb') as f:
                return f.read()
        except:
            return web.notfound()


class send_table(object):
    def GET(self, url):
        # Try to open the text file, returning a 404 upon failure
        title = os.path.basename(url)
        # if not title.endswith('.html'):
        #     return web.notfound(url)
        words, verses = self.get_table(url)
        if words is None:
            return web.notfound(url)

        return render.send_table(words, verses, title)

    def get_table(self, url):
        root, ext = os.path.splitext(url)
        book_name = os.path.dirname(root)
        pipe = redis.pipeline()
        pipe.hget(root, 'words')
        pipe.hget(root, 'ref')
        pipe.get(book_name)
        words, ref, book = pipe.execute()
        if words is None:
            return ["No {} found in {}.".format(os.path.basename(root), book_name)], [""]
        ref = json.loads(ref)
        book = json.loads(book)
        verses = [book[int(i)] for i in ref]
        return json.loads(words), verses


if __name__ == "__main__":
    application = web.application(urls, globals()).wsgifunc()
    print('Serving on 8080...')
    WSGIServer(('', 8080), application).serve_forever()
